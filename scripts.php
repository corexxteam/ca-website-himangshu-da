  <script src="https://www.google.com/recaptcha/api.js"></script>

  <script>
   function onSubmit(token) {
     document.getElementById("demo-form").submit();
   }
 </script>
 
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/jquery-2.2.4.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script type="text/javascript">
  	function loadcss(){
      var head  = document.getElementsByTagName('head')[0];
      var link  = document.createElement('link');
      link.rel  = 'stylesheet';
      link.type = 'text/css';
      link.href = 'mrincustom.css';
      link.media = 'all';
      head.appendChild(link);

      var head  = document.getElementsByTagName('head')[0];
      var link  = document.createElement('link');
      link.rel  = 'stylesheet';
      link.type = 'text/css';
      link.href = 'css/owl.carousel.css';
      link.media = 'all';
      head.appendChild(link);


  		var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('link');
	    link.rel  = 'stylesheet';
	    link.type = 'text/css';
	    link.href = 'css/bootstrap.min.css';
	    link.media = 'all';
	    head.appendChild(link);

	    var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('link');
	    link.rel  = 'stylesheet';
	    link.type = 'text/css';
	    link.href = 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css';
	    link.media = 'all';
	    head.appendChild(link);

  	}
  	loadcss();

  	$(document).ready(function(){
      $(".owl-theme").owlCarousel();
    });

    $('.owl-theme').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots:false,
        autoplay:true,
        animateOut: 'fadeOut',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    activePage = () => {
      let path = window.location.pathname;
      let str = path.split("/").pop();
      //alert( str );
      switch(str){
        case 'index.php': var element = document.getElementById("home")
                          element.classList.add("active")
                          break
        case 'about.php': var element = document.getElementById("about");
                          element.classList.add("active");
                          break;
        case 'contact.php': var element = document.getElementById("contact");
                          element.classList.add("active");
                          break;
        case 'partners.php': var element = document.getElementById("partner");
                          element.classList.add("active");
                          break;
        case 'about_GST.php': var element = document.getElementById("GST");
                          element.classList.add("active");
                          break;
        case 'GST_Registration.php': var element = document.getElementById("GST");
                          element.classList.add("active");
                          break;
        case 'GST_Return.php': var element = document.getElementById("GST");
                          element.classList.add("active");
                          break;
        case 'Assurance.php': var element = document.getElementById("services");
                          element.classList.add("active");
                          break;
      }

    }

    activePage()

  </script>