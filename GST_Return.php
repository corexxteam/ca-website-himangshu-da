<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <div class="container-xl">
  	<div class="py-4">
  	  <div class="row">
  	  	<div class="col-md-9">
          <!-- <img src="images/1.webp" alt="Manish Rajkumar & Co. (MRK) Image" style="float:right; margin-left:20px;margin-bottom:10px;"/> -->
          <h3 class="title-2">GST Return</h3>
  	  	  <p class="text-justify">ALP & Associates seeks people who can help us grow into one of the finest professional service organizations in the business world. We offer value-added services that make a perceptible impact in today's business. We believe that our focus on client satisfaction, upgradation of knowledge and investment in information technology will enable us to achieve our goals.</p><br/>
          <strong>Steps for filing GST return:</strong><br/>
          <ul class="gst-ul">
            <li><span>Eliminating cascading effect of taxes.</span></li>
            <li><span>Tax rates will be comparatively lower as the tax base will widen.</span></li>
            <li><span>Seamless flow of Input tax credit.</span></li>
            <li><span>Prices of the goods and services will fall.</span></li>
            <li><span>Efficient supply change management.</span></li>
              <li><span>Promote shift from unorganised sector to organised sector.</span></li>
          </ul>
  	    </div>
        <div class="col-md-3">
          <div class="gst-box">
            <img src="images/about_gst.jpg">
            <a href="about_GST.php"><h4 class="text-center mt-3">About GST</h4></a>
          </div>
          <div class="gst-box">
            <img src="images/gst_registration.jpg">
            <a href="GST_Registration.php"><h4 class="text-center mt-3">GST Registration</h4></a>
          </div>
          <div class="gst-box">
            <img src="images/gst_return.jpg">
            <a href="GST_Return"><h4 class="text-center mt-3">GST Return</h4></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>