<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <div class="container-xl">
  	<div class="py-4">
      <h1 class="text-center my-4 mb-4 title-1" style="color:var(--color-2);">OUR PARTNERS</h1>
  	  <div class="row">

  	  	<div class="col-md-12 partner">
          <div class="row">
            <div class="col-md-3 py-4">
              <img src="images/dp.png" style="width: 100%;height: auto;" alt="" />
            </div>
            <div class="col-md-9 p-2">
              <h3 class="title-2" style="margin-top:60px !important;">MANISH JAIN</h3>
            
              <strong>
                <p>
                  Ph: <a href="callto:9435018677">9435018677</a><br/>
                  E-mail: <a href="mailto:manish3jain@gmail.com">manish3jain@gmail.com</a>
                </p>
              </strong>
              <p class="my-2 text-justify">Mr. Manish Jain is founder and lead Partner of the firm. He is a graduate in commerce and also hold a bachelors degree in Law from Guwahati university and a fellow member of Institute of Chartered Accountants of India with 18 years standing in the profession. He has wide experience in the field of Direct Tax, Indirect taxes, Assurance and Accounting. Thus, Mr Manish Jain, apart from being a successful practicing chartered Accountant has proved his mettle in managing various business through his sheer managerial abilities and guidance. He is a man with high principles and business acumen and has always helped his clients in formulating the beneficial business strategies. He has always been proactive in identifying the business opportunities and has always successfully implemented the business strategy to perfection.</p>
            </div>
          </div>  
        </div>

        <div class="col-md-12 partner">
          <div class="row">
            <div class="col-md-3 py-4">
              <img src="images/dp.png" style="width: 100%;height: auto;" alt="" />
            </div>
            <div class="col-md-9 p-2">
              <h3 class="title-2" style="margin-top:60px !important;">MANOJ JAIN</h3>
            
              <strong>
                <p>
                  Ph: <a href="callto:+91-9706496490">+91-9706496490</a><br/>
                  E-mail: <a href="mailto:camanojjain.mrk@gmail.com">camanojjain.mrk@gmail.com</a>
                </p>
              </strong>
              <p class="my-2 text-justify">Manoj Jain, aged about 30 years is a practicing chartered accountant and has been practicing for 6 years. He completed his Graduation from Goenka College of Commerce and Business Administration, Kolkata. He is also a qualified Company Secretary and also holds a Post Qualification Diploma in Information Systems Audit (DISA) and Post Qualification Diploma in Insurance and Risk management(DIRM) from the Institute of Chartered Accountants of India.He specializes in Direct Taxes, Indirect Tax, Industrial and Investment Consultancy.</p>
            </div>
          </div>  
        </div>

        <div class="col-md-12 partner">
          <div class="row">
            <div class="col-md-3 py-4">
              <img src="images/dp.png" style="width: 100%;height: auto;" alt="" />
            </div>
            <div class="col-md-9 p-2">
              <h3 class="title-2" style="margin-top:60px !important;">SHANTI BHURAT</h3>
            
              <strong>
                <p>
                  Ph: <a href="callto:+91-9864219590">+91-9864219590</a>
                  Email: <a href="mailto:shantibhurat.mrk@gmail.com">shantibhurat.mrk@gmail.com</a>
                </p>
              </strong>
              <p class="my-2 text-justify">Shanti Bhurat, aged about 29 years is a practicing chartered accountant and has been practicing since 6 years. She completed her Masters in commerce from B.R.Mirdha college, Nagaur (Raj.). She is the Partner in the Firm M/s Manish Rajkumar & Co. She is the Partner in Charge of Kohima Office located at Amravati,Opp State Library, P.R Hills, Kohima. She specializes in Auditing & Assurance, Income tax and Project Consultancy. He has played a central Role in the audit of various major corporate and non-corporate clients. He has an extensive exposure of Indian Accounting, Auditing Standards, and Excise, VAT & Service Tax. She has vast experience in accountancy, taxation, Compliances and audits, GST, IFRS</p>
            </div>
          </div>  
        </div>

        <div class="col-md-12 partner">
          <div class="row">
            <div class="col-md-3 py-4">
              <img src="images/dp.png" style="width: 100%;height: auto;" alt="" />
            </div>
            <div class="col-md-9 p-2">
              <h3 class="title-2" style="margin-top:60px !important;">ARCHIE JAIN</h3>
            
              <strong>
                <p>
                  Ph: <a href="callto:+91-7002318190">+91-7002318190</a><br/>
                  Email: <a href="mailto:archiekashliwal@gmail.com">archiekashliwal@gmail.com</a>
                </p>
              </strong>
              <p class="my-2 text-justify">Archie Jain, aged about 26 years is a practicing chartered accountant and has been practicing for 3 years. She completed her Graduation from SPB College of Commerce, Surat, Gujrat. She also holds a Post Qualification Diploma in Insurance and Risk management(DIRM) from the Institute of Chartered Accountants of India. She is the Partner in Charge of Noida Office Located at 103 A, Ocean Complex, Sector 18, Noida U.P 201301. She specialises in Auditing and Assurance, Investment Consulting and Tax Planning Advisory.</p>
            </div>
          </div>  
        </div>


        <!-- <div class="col-md-10 partner">
          <div class="row">
            <h3 class="title-2 p-2" style="background:var(--color-2);color:white">MANOJ JAIN</h3>
            <hr/>
            <div class="col-md-3" style="border:1px solid #0005;">
              <img src="images/partner.png" style="min-height:200px;width:100%;" alt="" />
            </div>
            <div class="col-md-9">
              <strong>
                <p>
                  Ph: <a href="callto:+91-9706496490">+91-9706496490</a><br/>
                  E-mail: <a href="mailto:camanojjain.mrk@gmail.com">camanojjain.mrk@gmail.com</a>
                </p>
              </strong>
              <p class="my-2 text-justify">Manoj Jain, aged about 30 years is a practicing chartered accountant and has been practicing for 6 years. He completed his Graduation from Goenka College of Commerce and Business Administration, Kolkata. He is also a qualified Company Secretary and also holds a Post Qualification Diploma in Information Systems Audit (DISA) and Post Qualification Diploma in Insurance and Risk management(DIRM) from the Institute of Chartered Accountants of India.He specializes in Direct Taxes, Indirect Tax, Industrial and Investment Consultancy.</p>
            </div>
          </div>  
        </div>-->
      </div>
    </div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>