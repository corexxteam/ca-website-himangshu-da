<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <div class="container-xl">
  	<div class="py-4">
  	  <div class="row">
  	  	<div class="col-md-9">
          <!-- <img src="images/1.webp" alt="Manish Rajkumar & Co. (MRK) Image" style="float:right; margin-left:20px;margin-bottom:10px;"/> -->
          <h3 class="title-2">GST Registration</h3>
  	  	  <p class="text-justify">You can use Alp Associates for GST registration or you can do the registration via the GST portal yourself. To register for GST via Alp Associates, you just need to make payment and we will take care of the rest. To register yourself, you can follow the following steps:</p><br/>
          <strong>Who All Need To Get Gst Registration</strong><br/>
          <p class="text-justify">A business entity that is currently registered under any of the existing tax regimes then it is compulsorily required to migrate under GST law irrespective of the threshold limits. The following central and state level tax regimes will end with introduction of Goods and Service Tax (GST).</p><br/>
          <ul class="gst-ul">
            <li><span>Central Excise duty.</span></li>
            <li><span>Service Tax.</span></li>
            <li><span>State VAT.</span></li>
            <li><span>Central Sales Tax.</span></li>
            <li><span>Entry Tax (all forms).</span></li>
            <li><span>Luxury Tax.</span></li>
            <li><span>Entertainment and Amusement Tax (except when levied by the local bodies).</span></li>
            <li><span>Purchase Tax.</span></li>
            <li><span>State Surcharges and Cesses so far as they relate to supply of goods and services.</span></li>
            <li><span>Taxes on lotteries, betting and gambling.</span></li>
          </ul>
  	    </div>
        <div class="col-md-3">
          <div class="gst-box">
            <img src="images/about_gst.jpg">
            <a href="about_GST.php"><h4 class="text-center mt-3">About GST</h4></a>
          </div>
          <div class="gst-box">
            <img src="images/gst_registration.jpg">
            <a href="GST_Registration.php"><h4 class="text-center mt-3">GST Registration</h4></a>
          </div>
          <div class="gst-box">
            <img src="images/gst_return.jpg">
            <a href="GST_Return.php"><h4 class="text-center mt-3">GST Return</h4></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>