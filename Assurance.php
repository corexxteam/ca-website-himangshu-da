<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <div class="container-xl">
  	<div class="py-4">
  	  <div class="row" style="margin-bottom:30px;">
  	  	<div class="col-md-8">
          <!-- <img src="images/1.webp" alt="Manish Rajkumar & Co. (MRK) Image" style="float:right; margin-left:20px;margin-bottom:10px;"/> -->
          <h3 class="title-2" >Assurance</h3>
          <img src="images/assurance.png" style="float:left;width:500px;height:300px;" alt="Assurance">
  	  	  <p class="text-justify">Our Assurance practice provides high quality, independent audit services that transcends beyond conventional financial reporting.</p><br/>
          <p class="text-justify">We invest time and efforts to understand client’s business and needs and identify the key drivers, risks and opportunities. A comprehensive second partner review of audit engagement helps to assure you of your business scenario.</p><br/>
          <p class="text-justify">Refined over years, we deploy robust audit methodologies and bring in decades of insightful experience. Our people are highly skilled and trained to understand the nitty-gritties of your business and handle complexities. We employ people development programs to stay abreast of key issues, global best practices and latest technical updates (be it Accounting Standards or government notifications).</p><br/>
          <p class="text-justify">Our independent opinions, strong competency, value-addition approach and partnership philosophy makes us highly respected by our clientele, who range from PSUs, listed companies to SMEs.</p><br/>
        </div>
        <div class="col-md-4" style="padding-top:30px;">
          <h6 style="background-color: var(--color-2);color:white;width:100%;padding:10px;margin-bottom:30px;">Services</h6>
          <ul class="gst-ul">
            <li><span>Government/ Public Sector</span></li>
            <li><span>Undertaking (PSU) Audits</span></li>
            <li><span>Statutory Audits</span></li>
            <li><span>Tax Audits under Income Tax Law</span></li>
            <li><span>Tax Audits under VAT Law</span></li>
            <li><span>Internal Audits</span></li>
            <li><span>Certification Services</span></li>
            <li><span>Industrial Consultancy</span></li>
            <li><span>Legal Consultancy</span></li>
            <li><span>Corporate Compliance & planning</span></li>
            <li><span>Project Finance and management</span></li>
            <li><span>Consultancy</span></li>
            <li><span>PF and ESIC Consultancy</span></li>
            <li><span>Investment Consultancy</span></li>
          </ul>
  	    </div>
        <hr/>
        <?php include 'services.php'; ?>
      </div>
    </div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>