        <div class="col-md-12" style="margin:20px 0 0 0;">
          <h3 class="title-1" >Our Services</h3>
          <div class="row"style="margin:20px 0 0 0;">
            
            <div class="col-md-3">
              <div class="gst-box" style="box-shadow: 0px 0px 5px 0px #0002;">
                <img src="images/assurance.png" style="width:100%;height:150px;">
                <a href="Assurance.php"><h4 class="text-center mt-3">Assurance</h4></a>
              </div>
            </div>
            <div class="col-md-3">
              <div class="gst-box" style="box-shadow: 0px 0px 5px 0px #0002;">
                <img src="images/tax.jpg" style="width:100%;height:150px;">
                <a href="Tax.php"><h4 class="text-center mt-3">Tax</h4></a>
              </div>
            </div>
            <div class="col-md-3">
              <div class="gst-box" style="box-shadow: 0px 0px 5px 0px #0002;">           
                <img src="images/advisory.jpg" style="width:100%;height:150px;">
                <a href="Advisory.php"><h4 class="text-center mt-3">Advisory</h4></a>
              </div>
            </div>
            <div class="col-md-3">
              <div class="gst-box" style="box-shadow: 0px 0px 5px 0px #0002;">
                <img src="images/Industrial_Consultancy.jpg" style="width:100%;height:150px;">
                <a href="Industrial_Consultancy.php"><h4 class="text-center mt-3">Industrial Consultancy</h4></a>
              </div>
            </div>

          </div>
        </div>