<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <div class="container-xl">
  	<div class="py-4">
  	  <!-- <h1 class="text-center my-2 mb-4" style="color:var(--color-2);">CHARTERED ACCOUNTANT (CA) FIRM</h1> -->
  	  <div class="row">
  	  	<h1 class="title-1 my-4" style="font-size:32px;color:var(--color-1);margin-bottom: 38px !important;">Contact Us</h1>
  	  	<div class="col-md-6 col-lg-8 mb-4">	
  	      <div class="card text-center">
		    <div class="card-header" style="background: var(--color-2);color: white;margin:0;">
		      <h5 style="margin:0;">Head Office</h5>
		    </div>
		    <div class="card-body" style="text-align: left;">
		      <h5 class="card-title">GUWAHATI-3, RD Floor, GS Tower, Near Himmatsinghka Petrol pump, Chatribari Road, Guwahati 781001 Assam</h5>
		      <strong>
		        <p class="card-text">
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-telephone" viewBox="0 0 18 18">
  		            <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
		          </svg>
		          <a href="callto:+91-9435018677">+91-9435018677</a>
		        </p>
		        <p>
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-envelope" viewBox="0 0 18 18">
		      	    <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
		    	  </svg>
		          <a href="mailto:manish3jain@gmail.com">manish3jain@gmail.com</a>
		        </p>
		      </strong>
		    </div>
		  </div>

		    <div class="card text-center">
		    <div class="card-header" style="background: var(--color-2);color: white;margin:0;">
		      <h5 style="margin:0;">Branch Office</h5>
		    </div>
		    <div class="card-body" style="text-align: left;">
		      <h5>GS ROAD</h5>
		      <h6 class="card-title">1ST Floor, Above Green Micro Gym, GMCH Road,Near NEF Law College,Christian basti, Guwahati 781005 Assam</h6>
		      <strong>
		        <p class="card-text">
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-telephone" viewBox="0 0 18 18">
  		            <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
		          </svg>
		          <a href="callto:+9706496490">+9706496490/</a> <a a href="callto:2342121">2342121</a>
		        </p>
		        <p>
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-envelope" viewBox="0 0 18 18">
		      	    <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
		    	  </svg>
		          <a href="mailto:camanojjain.mrk@gmail.com">camanojjain.mrk@gmail.com</a>
		        </p>
		      </strong>
		      <hr/>
		      <h5>NOIDA</h5>
		      <h6 class="card-title">103 A, Ocean Complex, Sector 18, Noida U.P 201301</h6>
		      <strong>
		        <p class="card-text">
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-telephone" viewBox="0 0 18 18">
  		            <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
		          </svg>
		          <a href="callto:+9085572224">+9085572224</a>
		        </p>
		        <p>
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-envelope" viewBox="0 0 18 18">
		      	    <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
		    	  </svg>
		          <a href="mailto:archiekashliwal@gmail.com">archiekashliwal@gmail.com</a>
		        </p>
		      </strong>
		      <hr/>
		      <h5>KOHIMA</h5>
		      <h6 class="card-title">Amravati,Opp State Library, P.R Hills, Kohima-797005 Nagaland</h6>
		      <strong>
		        <p class="card-text">
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-telephone" viewBox="0 0 18 18">
  		            <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
		          </svg>
		          <a href="callto:+9085572224">+9085572224</a>
		        </p>
		        <p>
		      	  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-envelope" viewBox="0 0 18 18">
		      	    <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
		    	  </svg>
		          <a href="mailto:shantibhurat.mrk@gmail.com">shantibhurat.mrk@gmail.com</a>
		        </p>
		      </strong>
		    </div>
		  </div>

  	  	</div>
  	  	<div class="col-md-6 col-lg-4 mb-4">	
  	      <h1 class="title-1" style="background: var(--color-1);padding:9px 0;color:white;font-size:1.25rem;">Write To Us</h1>
  	      <form class="myform" id="demo-form" method="POST" action="enquiry.php">
			<div class="form-group">
			  <label for="exampleInputEmail1">Name *</label>
			  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
			</div>
			<div class="form-group">
			  <label for="exampleInputEmail1">Email Address *</label>
			  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
			</div>
			<div class="form-group">
			  <label for="exampleInputEmail1">Phone Number *</label>
			  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
			</div>
			<div class="form-group">
			  <label for="exampleInputEmail1">Massage</label>
			  <textarea class="form-control" rows="4"></textarea>
			</div>
			<!-- <button class="g-recaptcha btn btn-lg btn-primary" data-sitekey="6LfDuZoaAAAAALwZm8mdhEFFaULkS1WGqS3fsK3O" style="border:none; width:100%;background: var(--color-1);" data-callback='onSubmit' 
        data-action='submit'>Submit</button> -->
        <!-- <button class="g-recaptcha" 
        data-sitekey="6LfDuZoaAAAAALwZm8mdhEFFaULkS1WGqS3fsK3O" 
        data-callback='onSubmit' 
        data-action='submit'>Submit</button> -->
			<button type="submit" class="g-recaptcha btn btn-lg btn-primary" style="border:none; width:100%; background: var(--color-1);" data-sitekey="6LfDuZoaAAAAALwZm8mdhEFFaULkS1WGqS3fsK3O" 
        data-callback='onSubmit' 
        data-action='submit'>Submit</button>
		  </form>
  	  	</div>
  	  </div>
  	</div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>