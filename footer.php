<div class="container-fluid footer" style="font-family: 'Oswald';">
  <div class="container">
  	<div class="row">
  	  <div class="col-lg-3 col-md-3">
  		<h4>Our Services</h4>
  		<ul>
  		  <li><a class="text-white" href="#">Assurance</a></li>
          <li><a class="text-white" href="#">Tax</a></li>
          <li><a class="text-white" href="#">Advisory</a></li>
          <li><a class="text-white" href="#">Industrial Consultancy</a></li>
  		</ul>		
  	  </div>
  	  <div class="col-lg-3 col-md-3 my-1">
  		<h4>Important Links</h4>
  		<ul>
  		  <li><a class="text-white" href="#">About GST</a></li>
          <li><a class="text-white" href="#">GST Registration</a></li>
          <li><a class="text-white" href="#">GST Return</a></li>
          <li><a class="text-white" href="#">Sitemap</a></li>
  		</ul>
  	  </div>
  	  <div class="col-lg-3 col-md-3 my-1">
  	  	<h4>Head Office</h4>
		<p>GUWAHATI-3, RD Floor, GS Tower,Near Himmatsinghka Petrol pump, Chatribari Road, Guwahati 781001 Assam</P>
		<p>Ph: <a href="callto:+91-9435018677">+91-9435018677</a></p>
		<p>Email - <a href="mailto:manish3jain@gmail.com">manish3jain@gmail.com</a></p>
  	  </div>
  	  <div class="col-lg-3 col-md-3 my-1">
  	  	<h4>Branch Offices</h4>
  	  	<div class="tooltip1" style="width:100%;">GS ROAD
  	  	  <div class="tooltiptext1">
		    <p>1ST Floor, Above Green Micro Gym, GMCH Road,Near NEF Law College,Christian basti, Guwahati 781005 Assam</p>
		    <p>Ph: <a href="callto:+9706496490">+9706496490/</a> <a a href="callto:2342121">2342121</a></p>
		    <p>E-mail: <a href="mailto:camanojjain.mrk@gmail.com">camanojjain.mrk@gmail.com</a></p>
		  </div>
		</div>
		<div class="tooltip1" style="width:100%;">NOIDA
		  <div class="tooltiptext1">
		    <p>103 A, Ocean Complex, Sector 18, Noida U.P 201301</p>
		    <p>Ph: <a href="callto:+9085572224">+9085572224</a></p>
		    <p>E-mail: <a href="mailto:archiekashliwal@gmail.com">archiekashliwal@gmail.com</a></p>
		  </div>
		</div>
		<div class="tooltip1" style="width:100%;">KOHIMA
		  <div class="tooltiptext1">
		    <p>Amravati,Opp State Library, P.R Hills, Kohima-797005 Nagaland</p>
		    <p>Ph: <a href="callto:+9085572224">+9085572224</a></p>
		    <p>E-mail: <a href="mailto:shantibhurat.mrk@gmail.com">shantibhurat.mrk@gmail.com</a></p>
		  </div>
		</div>
  	  </div>
  	</div>
  </div>
  <hr/>
  <div class="container d-flex">
  	<span class="text-left">Designed & Developed By <a href="https://www.corexx.in" target="_blank" rel="noreferrer">Corexx</a></span>
  	<div class="ml-auto">&#169; 2021 MANISH RAJKUMAR & CO.</div>
  </div>
</div>

<a class="gotop" href="#">
  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="currentColor" class="bi bi-arrow-up-circle" viewBox="0 0 16 16">
    <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-7.5 3.5a.5.5 0 0 1-1 0V5.707L5.354 7.854a.5.5 0 1 1-.708-.708l3-3a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 5.707V11.5z"/>
  </svg>
</a> 