<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <div class="container-xl">
  	<div class="py-4">
  	  <div class="row">
  	  	<div class="col-md-11 m-auto">
          <img src="images/1.webp" alt="Manish Rajkumar & Co. (MRK) Image" style="float:right; margin-left:20px;margin-bottom:10px;"/>
          <h3 class="title-2">About Manish Rajkumar & Co. (MRK)</h3>
  	  	  <p class="text-justify">Founded in 2002, MANISH RAJKUMAR & CO. (MRK) is a Chartered Accountants firm providing Assurance, Taxation and Advisory services. Revered for our professional ethos and technical expertise, drawn on perspicacity of around 16 years and a team of highly competent professionals, we provide efficacious solutions to our client’s needs, running into deep engagements.</p><br/>
  	  	  <p class="text-justify">Our philosophy is of partnering with our clients and not being a distant service provider. Since businesses are inherently different, we tailor our services to meet client’s specific needs and banish the ‘one-size-fits-all’ standardisation.</p>
          <p class="text-justify">We recruit, train, motivate and retain highly capable and sharpest talent, who bring quality in their work and deliver the best solutions.</p><br/>
          <p class="text-justify">students of National Academy of Direct Taxes. The firm is one of a kind firm in Guwahati which offers umbrella service for its clients and offers specialized services in areas of Income Tax including handling Income Tax Hearing, TDS Matters, Service Tax, Excise, Project Financing, Industrial Consultancy, GST, PF ESIC and other allied areas. The Headquartered in Guwahati at 3rd Floor, G.S Tower, Near Himmatsinghka Petrol Pump, Chatribari Road, Guwahati 781001 Assam with branches at G.S Road at 1st Floor above Green Micro Gym, GMCH Road, Near NEF Law College, Christianbasti, Guwahati 781005, Kohima Branch at Amravati,Opp State Library, P.R Hills, Kohima and Noida Branch at 103 A, Ocean Complex, Sector 18, Noida U.P 201301. With these Expansions, we have leverage our state-of-art infrastructure, wide network, best practices and people development programs. Under the able direction of 4 partners, MRK’s team strength of over 55 people is uniquely positioned to provide you quality opinions and services. Our Interdisciplinary approach renders to give you seamless value. The firm has a very good credentials and reputation and has been among the top firms of Guwahati. The firm was also selected as a Model office in the North Eastern region by the Income Tax Department, Guwahati for imparting training to the freshly recruited Income Tax Officers and firm has always been pioneer in adopting the most recent technologies in the field of Chartered Accountancy profession</p><br/>
          <p class="text-justify">Serving to the wider business community since more than 18 years, we enjoy unparalleled reputation and respect of our clients, who trust and rely on us for our expertise and professionalism.</p>
  	    </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>