<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <div class="container-xl">
  	<div class="py-4">
  	  <div class="row">
  	  	<div class="col-md-9">
          <!-- <img src="images/1.webp" alt="Manish Rajkumar & Co. (MRK) Image" style="float:right; margin-left:20px;margin-bottom:10px;"/> -->
          <h3 class="title-2">About GST</h3>
  	  	  <p class="text-justify">The Prime Minister approved the constitution amendment bill for Goods and Service Tax(GST) in the Parliament Session (Rajya Sabha on 3 August 2016 and Lok Sabha on 8 August 2016) along with the ratification by 50 percent of state legislatures. Thus the current indirect taxes levied by state and centre are all set to be replaced with proposed implementation of GST by April 2017.</p><br/>
          <p class="text-justify">This would be the biggest tax reform since independence and a boon to the economy as it will eradicate the shortcomings of the current tax structure and provide a single tax on supply of all goods and services.</p><br/>
          <strong>Benefits of GST</strong><br/>
          <ul class="gst-ul">
            <li><span>Eliminating cascading effect of taxes.</span></li>
            <li><span>Tax rates will be comparatively lower as the tax base will widen.</span></li>
            <li><span>Seamless flow of Input tax credit.</span></li>
            <li><span>Prices of the goods and services will fall.</span></li>
            <li><span>Efficient supply change management.</span></li>
            <li><span>Promote shift from unorganised sector to organised sector.</span></li>
          </ul>
  	    </div>
        <div class="col-md-3">
          <div class="gst-box">
            <div style="box-shadow: 0px 0px 5px 0px #0002;">
              <img src="images/about_gst.jpg">
              <a href="about_GST.php"><h4 class="text-center mt-3">About GST</h4></a>
            </div>
          </div>
          <div class="gst-box">
            <div style="box-shadow: 0px 0px 5px 0px #0002;">
              <img src="images/gst_registration.jpg">
              <a href="GST_Registration.php"><h4 class="text-center mt-3">GST Registration</h4></a>
            </div>
          </div>
          <div class="gst-box">
            <div style="box-shadow: 0px 0px 5px 0px #0002;">
              <img src="images/gst_return.jpg">
              <a href="GST_Return"><h4 class="text-center mt-3">GST Return</h4></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>