<!DOCTYPE html>
<html>
<head>
	<?php include 'head.php'; ?>
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="mrincustom.css"> -->
</head>
<body>
  <?php include 'navbar.php';?>
  <?php include 'slide.php'; ?>
  <div class="container-xl mb-4">
  	<div class="py-4">
  	  <h1 class="title-1">CHARTERED ACCOUNTANT (CA) FIRM</h1>
  	  <div class="row">
  	  	<div class="col-md-6">
  	  	  <h3 class="title-2">About Manish Rajkumar & Co. (MRK)</h3>
  	  	  <p class="text-justify">Founded in 2002, MANISH RAJKUMAR & CO. (MRK) is a Chartered Accountants firm providing Assurance, Taxation and Advisory services. Revered for our professional ethos and technical expertise, drawn on perspicacity of around 16 years and a team of highly competent professionals, we provide efficacious solutions to our client’s needs, running into deep engagements.</p><br/>
  	  	  <p class="text-justify">Our philosophy is of partnering with our clients and not being a distant service provider. Since businesses are inherently different, we tailor our services to meet client’s specific needs and banish the ‘one-size-fits-all’ standardisation.</p>
  	  	  <a href="about.php"><button class="btn btn-rounded btn-primary mt-2" style="background: var(--color-1);border:none;">read more</button></a>
  	  	</div>
  	  	<div class="col-md-6">
  	  	  <img src="images/1.webp" alt="Manish Rajkumar & Co. (MRK) Image"/>
  	  	</div>
  	  </div>
      <hr/>
      <h1 class="text-center my-4 mb-4 title-1" style="color:white;background: var(--color-2);padding:5px 0;">OUR VALUES</h1>
      <div class="row">
        <div class="col-md-12 m-auto" style="margin-bottom:32px !important;">
          <p class="text-justify">Our philosophy, principles and values are so strongly weaved in our culture fabric that our beliefs are shared amongst all and which helps us earn our client’s trust and respect. We are pioneers in driving all legal and compliance services under one single umbrella which caters to the complete lifecycle of regulatory compliances. The Firm has rendered services to thousands of clients spread across India Technology adoption at MRK goes a long way to build a future-ready tax and law firm. The group has always been pioneer in expanding the business verticals so as to provide the umbrella service thereby providing synergic benefits to all its clients under one room through its various Group Associates</p>
        </div>
        <div class="col-md-3">
          <img src="images/partner.png" class="values-icon"/>
          <h3 class="title-2 text-center">Partnership</h3>
          <p class="text-justify">Instead of being a distant service provider, we collaborate with our clients in all our engagements, work with them as a team and take ownership and responsibility of things, to create long lasting partnerships.</p>
        </div>
        <div class="col-md-3">
          <img src="images/integrity.png" class="values-icon"/>
          <h3 class="title-2 text-center">Integrity</h3>
          <p class="text-justify">Our services are aimed at protecting our client’s interests. By adopting transparent processes and adhering to highest ethical standards, we ensure client confidentiality and our own credibility. Whilst collaborating with our clients, we remain absolutely independent to deliver unbiased opinions.</p>
        </div>
        <div class="col-md-3">
          <img src="images/passion.png" class="values-icon"/>
          <h3 class="title-2 text-center">Passion</h3>
          <p class="text-justify">We are passionate for our client’s success. By creating a highly stimulating work environment, working with utmost dedication and commitment and focusing on delivery and execution, we perform to not just satisfy but delight our clients.</p>
        </div>
        <div class="col-md-3">
          <img src="images/excelence.png" class="values-icon"/>
          <h3 class="title-2 text-center">Excellence</h3>
          <p class="text-justify">By continually focusing on quality and deploying best practices, we bring excellence in our work, add value for our clients and strive to enter the realm of supremacy.</p>
        </div>
        <?php include 'services.php'; ?>
      </div>
    </div>
  </div>
  <?php include 'footer.php';?>
  <?php include 'scripts.php';?>
</body>
</html>