  <div class="headnav">
  	<div class="container">
      <div class="row">
  	  <div class="col-lg-3">
  	 
  	  	CALL : 
  	  	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone" viewBox="0 0 18 18">
  		    <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
		    </svg>
		    <a href="callto:+91-9435018677">+91-9435018677</a>
	    </div>
	    <div class="col-lg-5">
  	  
  	    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="18" fill="currentColor" class="bi bi-envelope" viewBox="0 0 18 18">
		      <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
		    </svg>
	 
	      MAIL US AT : <a href="mailto:manish3jain@gmail.com">manish3jain@gmail.com</a>
	    </div>
	    <div class="col-lg-4 ml-auto text-right">
	  	  CONNECT US ON 
	  	  <ul class="socio-icons">
	  	    <li class="social-icon facebook">
	  	  	  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
		  	      <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
			      </svg>
		      </li>
		      <li class="social-icon twitter">
	  	  	  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-twitter" viewBox="0 0 16 16">
			        <path d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z"/>
			      </svg>
		      </li>
		    </ul>
	    </div>
      </div>
	  </div>
  </div>
  
  <nav class="navbar navbar-expand-lg navbar-light bg-light" aria-label="Ninth navbar example">
    <div class="container-xl">
      <a class="navbar-brand" href="#">
      	MANISH RAJKUMAR & CO.
      	<!-- <img src="" class="navbar-logo" style="height:100px;" alt="logo"/> -->
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample07XL">
        <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
          <li class="nav-item" id="home">
            <a class="nav-link" href="index.php">Home</a>
          </li>
           <li class="nav-item" id="about">
            <a class="nav-link" href="about.php">About Us</a>
          </li>
  		  <li class="nav-item dropdown" id="GST">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown1" data-bs-toggle="dropdown" aria-expanded="false">GST</a>
            <ul class="dropdown-menu" aria-labelledby="dropdown1">
              <li><a class="dropdown-item" href="about_GST.php">About GST</a></li>
              <li><a class="dropdown-item" href="#">GST Registration</a></li>
              <li><a class="dropdown-item" href="#">GST Return</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown" id="services">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown2" data-bs-toggle="dropdown" aria-expanded="false">Services</a>
            <ul class="dropdown-menu" aria-labelledby="dropdown2">
              <li><a class="dropdown-item" href="Assurance.php">Assurance</a></li>
              <li><a class="dropdown-item" href="Tax.php">Tax</a></li>
              <li><a class="dropdown-item" href="Advisory.php">Advisory</a></li>
              <li><a class="dropdown-item" href="Industrial_Consultancy.php">Industrial Consultancy</a></li>
            </ul>
          </li>
          <li class="nav-item" id="partner">
            <a class="nav-link" href="partners.php">Partners</a>
          </li>
          <li class="nav-item" id="contact">
            <a class="nav-link" href="contact.php">Contact Us</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>